#!/bin/sh

# Create MySQL dump if configured
if [ "${MYSQL_DUMP_CREATE}" != "false" ]; then
	/usr/bin/mysqldump --user=${MYSQL_DUMP_USER} --password=${MYSQL_DUMP_PASS} --host=${MYSQL_DUMP_HOST} ${MYSQL_DUMP_DB} > ${MYSQL_DUMP_PATH}
fi

# sed substitutions in MySQL dump


# Import of MySQL dump
/usr/bin/mysql --user=${MYSQL_IMPORT_USER} --password=${MYSQL_IMPORT_PASS} --host=${MYSQL_IMPORT_HOST} --execute="drop database if exists ${MYSQL_IMPORT_DB};"
/usr/bin/mysql --user=${MYSQL_IMPORT_USER} --password=${MYSQL_IMPORT_PASS} --host=${MYSQL_IMPORT_HOST} --execute="create database ${MYSQL_IMPORT_DB};"
/usr/bin/mysql --user=${MYSQL_IMPORT_USER} --password=${MYSQL_IMPORT_PASS} --host=${MYSQL_IMPORT_HOST} ${MYSQL_IMPORT_DB} < ${MYSQL_DUMP_PATH}

# SQL statements


# Adjustments in home directory


